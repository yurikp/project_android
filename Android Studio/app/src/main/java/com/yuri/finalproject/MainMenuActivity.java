package com.yuri.finalproject;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.*;

import com.google.android.gms.ads.AdView;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.*;
import com.google.android.gms.games.Games;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.*;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class MainMenuActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {

    private static final String TAG = "MenuActivity";
    private Button mPlay, mHighScore, mExit, credit;
    private static final int RC_SIGN_IN = 9001;
    private static final int RC_LEADERBOARD_UI = 9004;
    private static final int RC_ACHIEVEMENT_UI = 9003;
    final public int BUTTON_SIGN_IN = 2;
    final public int BUTTON_SIGN_OUT = 1;
    private TextView mStatusText;

    SignInButton signInButton;
    private GoogleSignInAccount googleSignInAccount = null;
    private GoogleSignInClient googleSignInClient;
    private FirebaseAuth firebaseAuth;
    private GoogleApiClient apiClient;

    private AdView adView;
    MainActivity mainActivity;
    private Object GoogleSignInClient;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        //Membuat tampilan menjadi full screen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //Membuat tampilan selalu menyala jika activity aktif
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        mPlay = findViewById(R.id.play);
        mHighScore = findViewById(R.id.high_score);
        credit = findViewById(R.id.credit);
        mExit = findViewById(R.id.exit);
//        mStatusText = findViewById(R.id.status);
//        findViewById(R.id.disconnect_button).setOnLongClickListener(this);
        signInButton = (SignInButton) findViewById(R.id.sign_in_button);

        mPlay.setOnClickListener(this);
        mHighScore.setOnClickListener(this);
        credit.setOnClickListener(this);
        mExit.setOnClickListener(this);
        signInButton.setOnClickListener(this);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_GAMES_SIGN_IN)
                .requestEmail()
                .build();

        apiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

//        Games.getLeaderboardsClient(this, GoogleSignIn.getLastSignedInAccount(this))
//                .submitScore(getString(R.string.leaderboard), 1337);

//        Games.getAchievementsClient(this, GoogleSignIn.getLastSignedInAccount(this))
//                .unlock(getString(R.string.achivement_welcome));

    }

    private void SignIn(){
        Intent intent = Auth.GoogleSignInApi.getSignInIntent(apiClient);
        startActivityForResult(intent, RC_SIGN_IN);
    }

    private void showLead(){
        Games.getLeaderboardsClient(this, (GoogleSignIn.getLastSignedInAccount(this)))
                .getLeaderboardIntent(getString(R.string.leaderboard))
                .addOnSuccessListener(new OnSuccessListener<Intent>() {
                    @Override
                    public void onSuccess(Intent intent) {
                        startActivityForResult(intent, RC_LEADERBOARD_UI);
                    }
                });
    }

    private void showAchievements() {
        Games.getAchievementsClient(this, (GoogleSignIn.getLastSignedInAccount(this)))
                .getAchievementsIntent()
                .addOnSuccessListener(new OnSuccessListener<Intent>() {
                    @Override
                    public void onSuccess(Intent intent) {
                        startActivityForResult(intent, RC_ACHIEVEMENT_UI);
                    }
                });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.sign_in_button:
                Log.d(TAG,"BUTTON_SIGN_IN");
                Toast.makeText(this, "SIGN IN", Toast.LENGTH_SHORT).show();

                SignIn();
                showAchievements();
                break;
            case R.id.play:
                Log.d(TAG,"PLAY");
                Toast.makeText(this, "PLAY", Toast.LENGTH_SHORT).show();

                startActivity(new Intent(this, MainActivity.class));
                finish();
                break;
            case R.id.high_score:
                Log.d(TAG,"LEADERBOARD");
                Toast.makeText(this, "LEADERBOARD", Toast.LENGTH_SHORT).show();

//                startActivity(new Intent(this, HighScoreActivity.class));
                showLead();
                break;
            case R.id.credit:
                Log.d(TAG,"CREDIT");
                Toast.makeText(this, "CREDIT", Toast.LENGTH_SHORT).show();

                startActivity(new Intent(this, CreditActivity.class));
                break;
            case R.id.exit:
                Log.d(TAG,"EXIT");
                Toast.makeText(this, "EXIT", Toast.LENGTH_SHORT).show();

                finish();
                break;
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==RC_SIGN_IN){
//            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
//            handleSignInResult(result);
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

//    private void handleSignInResult(GoogleSignInResult result){
//        if(result.isSuccess()){
//            gotoGame();
//        }else{
//            Toast.makeText(getApplicationContext(),"Sign in cancel",Toast.LENGTH_LONG).show();
//        }
//    }
//    private void gotoGame(){
//        Intent intent = new Intent(MainMenuActivity.this, GameView.class);
//        startActivity(intent);
//    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
            updateUI(account);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
            updateUI(null);
        }
    }

    private void updateUI(@Nullable GoogleSignInAccount account) {
        if (account != null) {
//            mStatusText.setText(getString(R.string.signed_in_fmt, account.getDisplayName()));

            findViewById(R.id.sign_in_button).setVisibility(View.GONE);
//            findViewById(R.id.sign_out_and_disconnect).setVisibility(View.VISIBLE);
        } else {
            mStatusText.setText(R.string.signed_out);

            findViewById(R.id.sign_in_button).setVisibility(View.VISIBLE);
//            findViewById(R.id.sign_out_and_disconnect).setVisibility(View.GONE);
        }
    }
}
